﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace battlePointer
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

        private void PlusButtonClickedA(object sender, EventArgs e)
        {
            int pointA;
            bool success = int.TryParse(this.playerPointA.Text, out pointA);

            if (success)
            {
                int taxPointA = (int)(pointA + 1);
                this.playerPointA.Text = taxPointA.ToString();
            }
        }

        private void MinusButtonClickedA(object sender, EventArgs e)
        {
            int pointA;
            bool success = int.TryParse(this.playerPointA.Text, out pointA);

            if (success)
            {
                int taxPointA = (int)(pointA - 1);
                this.playerPointA.Text = taxPointA.ToString();
            }
        }

        private void PlusButtonClickedB(object sender, EventArgs e)
        {
            int pointB;
            bool success = int.TryParse(this.playerPointB.Text, out pointB);

            if (success)
            {
                int taxPointB = (int)(pointB + 1);
                this.playerPointB.Text = taxPointB.ToString();
            }
        }

        private void MinusButtonClickedB(object sender, EventArgs e)
        {
            int pointB;
            bool success = int.TryParse(this.playerPointB.Text, out pointB);

            if (success)
            {
                int taxPointB = (int)(pointB - 1);
                this.playerPointB.Text = taxPointB.ToString();
            }
        }
    }
}
