﻿namespace battlePointer
{
	partial class Form1
	{
		/// <summary>
		/// 必要なデザイナー変数です。
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 使用中のリソースをすべてクリーンアップします。
		/// </summary>
		/// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows フォーム デザイナーで生成されたコード

		/// <summary>
		/// デザイナー サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディターで変更しないでください。
		/// </summary>
		private void InitializeComponent()
		{
            this.plus1A = new System.Windows.Forms.Button();
            this.minus1A = new System.Windows.Forms.Button();
            this.plus1B = new System.Windows.Forms.Button();
            this.minus1B = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.playerPointA = new System.Windows.Forms.Label();
            this.playerPointB = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // plus1A
            // 
            this.plus1A.Font = new System.Drawing.Font("MS UI Gothic", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.plus1A.Location = new System.Drawing.Point(27, 153);
            this.plus1A.Name = "plus1A";
            this.plus1A.Size = new System.Drawing.Size(50, 50);
            this.plus1A.TabIndex = 0;
            this.plus1A.Text = "+1";
            this.plus1A.UseVisualStyleBackColor = true;
            this.plus1A.Click += new System.EventHandler(this.PlusButtonClickedA);
            // 
            // minus1A
            // 
            this.minus1A.Font = new System.Drawing.Font("MS UI Gothic", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.minus1A.Location = new System.Drawing.Point(94, 153);
            this.minus1A.Name = "minus1A";
            this.minus1A.Size = new System.Drawing.Size(50, 50);
            this.minus1A.TabIndex = 1;
            this.minus1A.Text = "-1";
            this.minus1A.UseVisualStyleBackColor = true;
            this.minus1A.Click += new System.EventHandler(this.MinusButtonClickedA);
            // 
            // plus1B
            // 
            this.plus1B.Font = new System.Drawing.Font("MS UI Gothic", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.plus1B.Location = new System.Drawing.Point(280, 153);
            this.plus1B.Name = "plus1B";
            this.plus1B.Size = new System.Drawing.Size(50, 50);
            this.plus1B.TabIndex = 2;
            this.plus1B.Text = "+1";
            this.plus1B.UseVisualStyleBackColor = true;
            this.plus1B.Click += new System.EventHandler(this.PlusButtonClickedB);
            // 
            // minus1B
            // 
            this.minus1B.Font = new System.Drawing.Font("MS UI Gothic", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.minus1B.Location = new System.Drawing.Point(352, 153);
            this.minus1B.Name = "minus1B";
            this.minus1B.Size = new System.Drawing.Size(50, 50);
            this.minus1B.TabIndex = 3;
            this.minus1B.Text = "-1";
            this.minus1B.UseVisualStyleBackColor = true;
            this.minus1B.Click += new System.EventHandler(this.MinusButtonClickedB);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(187, 74);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 40);
            this.label1.TabIndex = 4;
            this.label1.Text = "―";
            // 
            // playerPointA
            // 
            this.playerPointA.Font = new System.Drawing.Font("MS UI Gothic", 65F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.playerPointA.Location = new System.Drawing.Point(4, 51);
            this.playerPointA.Name = "playerPointA";
            this.playerPointA.Size = new System.Drawing.Size(177, 87);
            this.playerPointA.TabIndex = 5;
            this.playerPointA.Text = "0";
            this.playerPointA.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // playerPointB
            // 
            this.playerPointB.Font = new System.Drawing.Font("MS UI Gothic", 60F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.playerPointB.Location = new System.Drawing.Point(250, 57);
            this.playerPointB.Name = "playerPointB";
            this.playerPointB.Size = new System.Drawing.Size(176, 80);
            this.playerPointB.TabIndex = 6;
            this.playerPointB.Text = "0";
            this.playerPointB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(426, 219);
            this.Controls.Add(this.playerPointB);
            this.Controls.Add(this.playerPointA);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.minus1B);
            this.Controls.Add(this.plus1B);
            this.Controls.Add(this.minus1A);
            this.Controls.Add(this.plus1A);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button plus1A;
		private System.Windows.Forms.Button minus1A;
		private System.Windows.Forms.Button plus1B;
		private System.Windows.Forms.Button minus1B;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label playerPointA;
		private System.Windows.Forms.Label playerPointB;
	}
}

